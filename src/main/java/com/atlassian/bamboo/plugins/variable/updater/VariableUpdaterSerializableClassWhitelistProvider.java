package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.plugins.variable.updater.message.UpdatePlanVariableMessage;
import com.atlassian.bamboo.security.SerializableClassWhitelistProvider;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class VariableUpdaterSerializableClassWhitelistProvider implements SerializableClassWhitelistProvider
{
    private static final List<String> WHITELISTED_CLASSES = Arrays.asList(
            UpdatePlanVariableMessage.class.getName()
    );

    @NotNull
    @Override
    public Iterable<String> getWhitelistedClasses()
    {
        return WHITELISTED_CLASSES;
    }
}
