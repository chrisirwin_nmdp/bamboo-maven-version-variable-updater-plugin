package com.atlassian.bamboo.plugins.variable.updater.task.executor;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskType;
import com.atlassian.bamboo.plugins.variable.updater.VariableUpdater;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.remote.RemoteAgent;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.atlassian.bamboo.variable.VariableType;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public abstract class VersionVariableTask implements TaskType, DeploymentTaskType {
   public static final String VARIABLE_CONFIG_KEY = "variable";
   public static final String INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY = "includeGlobals";
   public static final String OVERRIDE_BRANCH_VARIABLE = "branchVars";
   public static final String KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY = "overrideCustomised";
   public static final String SCOPE_VARIABLE_CONFIG_KEY = "variableScope";

   @NotNull
   @java.lang.Override
   public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {
      return executeTask(taskContext);
   }

   @NotNull
   @Override
   public TaskResult execute(@NotNull final DeploymentTaskContext taskContext) throws TaskException {
      return executeTask(taskContext);
   }

   public abstract TaskResult executeTask(CommonTaskContext taskContext) throws TaskException;

   public TaskResult skipUpdatingIfConflictWithCustomVariables(final CommonTaskContext taskContext,
         final String variableName) {
      taskContext.getBuildLogger()
            .addBuildLogEntry("Variable: " + variableName + " already been given a customised value, skip updating");
      return TaskResultBuilder.newBuilder(taskContext).success().build();
   }

   String retrieveCurrentValue(String variableName, Map<String, String> buildCustomVariables,
         VariableContext variableContext, BuildLogger buildLogger) {
      if (buildCustomVariables.containsKey(variableName)) {
         return buildCustomVariables.get(variableName);
      }
      if (variableContext.getDefinitions().containsKey(variableName)) {
         return variableContext.getDefinitions().get(variableName).getValue();
      }
      buildLogger.addBuildLogEntry("Variable wasn't found in the Build context. It possibly doesn't exist. ");
      return "";
   }

   VariableUpdater.SAVE_STRATEGY retrieveStrategy(final String variableName, final boolean keepCustomised,
         final VariableContext context) {
      final VariableDefinitionContext variableContext = context.getDefinitions().get(variableName);
      return (keepCustomised && variableContext != null && variableContext.getVariableType() == VariableType.MANUAL)
            ? VariableUpdater.SAVE_STRATEGY.SKIP : VariableUpdater.SAVE_STRATEGY.READ_SAVE;
   }

   boolean isRemote() {
      try {
         return (RemoteAgent.getContext() != null);
      } catch (IllegalStateException e) {
         return false;
      }
   }
}
