package com.atlassian.bamboo.plugins.variable.updater.task.executor;

import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.ResultKey;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.plugins.variable.updater.VariableUpdater;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableType;

public class AdvanceVersionVariableTask extends VersionVariableTask
{
    public static final String VERSION_PATTERN_TO_MATCH_CONFIG_KEY = "versionPattern";
    public static final String UPDATE_STRATEGY_CONFIG_KEY = "strategy";

    @NotNull
    @Override
    public TaskResult executeTask(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final String variableName = taskContext.getConfigurationMap().get(VARIABLE_CONFIG_KEY);
        final String variableValueRegex = taskContext.getConfigurationMap().get(VERSION_PATTERN_TO_MATCH_CONFIG_KEY);
        final VariableUpdater.SCOPE variableScope = VariableUpdater.SCOPE.valueOf(taskContext.getConfigurationMap().get(SCOPE_VARIABLE_CONFIG_KEY).toUpperCase());
        final VariableUpdater.INCREMENT_STRATEGY strategy;
        final boolean includeGlobals = Boolean.parseBoolean(taskContext.getConfigurationMap().get(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));
        final boolean useBranchVariables = Boolean.parseBoolean(taskContext.getConfigurationMap().get(OVERRIDE_BRANCH_VARIABLE));

        final VariableContext variableContext = taskContext.getCommonContext().getVariableContext();
        final VariableType variableType = variableContext.getDefinitions().get(variableName) == null? VariableType.UNKNOWN : variableContext.getDefinitions().get(variableName).getVariableType();
        final boolean keepCustomised = Boolean.parseBoolean(taskContext.getConfigurationMap().get(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY));
        if ( keepCustomised && variableType == VariableType.MANUAL){
            return skipUpdatingIfConflictWithCustomVariables(taskContext, variableName);
        } else {
            strategy = VariableUpdater.INCREMENT_STRATEGY.valueOf(taskContext.getConfigurationMap().get(UPDATE_STRATEGY_CONFIG_KEY).toUpperCase());
        }


        final String planKeyOrEnvironmentId;
        final ResultKey resultKey;
        if (taskContext instanceof DeploymentTaskContext){
            final DeploymentTaskContext deploymentTaskContext = (DeploymentTaskContext) taskContext;
            final long deploymentEnvironmentId = deploymentTaskContext.getDeploymentContext().getEnvironmentId();
            planKeyOrEnvironmentId = String.valueOf(deploymentEnvironmentId);
            resultKey = deploymentTaskContext.getDeploymentContext().getResultKey();

        } else if (taskContext instanceof TaskContext){
            final TaskContext tContext = (TaskContext) taskContext;
            planKeyOrEnvironmentId = tContext.getBuildContext().getParentBuildContext().getPlanKey();
            resultKey = tContext.getBuildContext().getResultKey();

        } else {
            throw new TaskException("TaskContext not defined: " + taskContext.getClass().getName());
        }

        final Map<String, String> buildCustomVariables = taskContext.getCommonContext().getCurrentResult()
                .getCustomBuildData();

        final String currentValue = retrieveCurrentValue(variableName, buildCustomVariables, variableContext,
                taskContext.getBuildLogger());

        VariableUpdater updater = new VariableUpdater(resultKey, taskContext.getWorkingDirectory().getAbsolutePath(), buildCustomVariables);
        updater.update(planKeyOrEnvironmentId, variableName, currentValue, variableValueRegex, includeGlobals,
            variableScope, strategy, isRemote(), taskContext.getBuildLogger(), variableContext, useBranchVariables);

        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }


}